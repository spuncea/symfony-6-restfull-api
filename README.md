# RESTFull API using Symfony 6, Docker 

## What is this?
This is a project which includes `Symfony 6.0` and a `Docker configuration`.

It contains features needed for a Restfull API:
- Authentication system with Json Web Tokens (JWT) (https://github.com/lexik/LexikJWTAuthenticationBundle)
- Endpoints to manage users, companies and projects (CRUD)
- Fixtures


## Folder structure
```
src
├───Api/
│   ├───Company/ <- Company endpoints & data transformers
│   ├───Project/ <- Project endpoints  & data transformers
│   ├───Response/ <- Beautify api response
│   ├───User/ <- User endpoints  & data transformers
├───DataFixtures/ <- Start data for the API
├───Entity/ <- Doctrine entities
├───Exception/ <- Domain exceptions
├───Migrations/ <- Migrations (new migrations will be added here)
├───Repository/ <- Entity repositories
├───Security/
│   ├───Factory/ <- Some factories 
│   ├───Listener/ <- Custom exception listeners
│   ├───User/ <- User provider and role
├───Service/ <- App services go here
```

## Usage
- `make build` to build the docker environment
- `make run` to spin up containers
- `make prepare` to install dependencies and run migrations & fixtures
- `make generate-ssh-keys` to generate JWT certificates
- API endpoints will be available at `http://localhost:8080/`
- In order to make request to endpoints you need to be authenticated.
- To authenticate, you can retrieve a Bearer token for the default user using curl:

```
curl --location --request POST 'http://localhost:8080/api/login_check' \
--header 'Content-Type: application/json' \
--header 'Accept: application/json' \
--data-raw '{"username": "restfulapi@example.com","password":"password"}'
```
- Example curl request to retrieve all companies: 

```
curl --location --request GET 'http://localhost:8080/api/companies' \
--header 'Content-Type: application/json' \
--header 'Accept: application/json' \
--header 'Authorization: Bearer ${token}' \
--data-raw '{"username":"restfulapi@example.com","password":"password"}'
```

- `make restart` to stop and start containers
- `make ssh-be` to access the PHP container bash
- `make be-logs` to tail dev logs



## Stack:
- `NGINX 1.19` container
- `PHP 8.02 FPM` container
- `MySQL 8.0` container + `volume`

