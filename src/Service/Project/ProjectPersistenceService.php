<?php

namespace App\Service\Project;

use App\Entity\Project;
use App\Repository\ProjectRepository;

class ProjectPersistenceService
{
    protected ProjectRepository $projectRepository;

    public function __construct(ProjectRepository $projectRepository)
    {
        $this->projectRepository = $projectRepository;
    }

    public function createProject(Project $project)
    {
        $this->projectRepository->save($project, true);
    }

    public function deleteProject(Project $project)
    {
        $this->projectRepository->remove($project, true);
    }
    
}