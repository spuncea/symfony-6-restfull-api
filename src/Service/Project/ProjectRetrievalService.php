<?php

namespace App\Service\Project;

use App\Entity\Project;
use App\Repository\ProjectRepository;

class ProjectRetrievalService
{
    protected ProjectRepository $projectRepository;

    public function __construct(ProjectRepository $projectRepository)
    {
        $this->projectRepository = $projectRepository;
    }

    public function retrieveOneById($id): ?Project
    {
        return $this->projectRepository->find($id);
    }
}