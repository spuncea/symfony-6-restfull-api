<?php

namespace App\Service\User;

use App\Api\User\UserDataTransformer;
use App\Entity\User;
use App\Repository\UserRepository;

class UserRetrievalService
{
    protected UserRepository $userRepository;
    protected UserDataTransformer $dataTransformer;
    
    public function __construct(UserRepository $userRepository, UserDataTransformer $dataTransformer)
    {
        $this->userRepository = $userRepository;
        $this->dataTransformer = $dataTransformer;
    }

    /**
     * Get all users 
     * 
     * @return array
     */
    public function getUsers(): array
    {
        $users = $this->userRepository->findAll();
        return $this->dataTransformer->transformList($users);
    }

    public function retrieveOneByEmailOrFail($email): ?User
    {
        return $this->userRepository->findOneByEmailOrFail($email);
    }

    public function retrieveOneByEmail($email): ?User
    {
        return $this->userRepository->findOneByEmail($email);
    }

    public function retrieveOneById($id): ?User
    {
        return $this->userRepository->find($id);
    }

}