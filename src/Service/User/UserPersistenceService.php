<?php

namespace App\Service\User;

use App\Entity\User;
use App\Repository\UserRepository;

class UserPersistenceService
{
    protected UserRepository $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function createUser(User $user)
    {
        $this->userRepository->save($user, true);
    }

    public function deleteUser(User $user)
    {
        $this->userRepository->remove($user, true);
    }

    public function updateUser(User $user)
    {
        $this->userRepository->save($user);
    }
    
}