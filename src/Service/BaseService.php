<?php

namespace App\Service;

use App\Repository\CompanyRepository;
use App\Service\Company\CompanyRetrievalService;

class BaseService
{
    protected CompanyRepository $companyRepository;
    protected CompanyRetrievalService $companyRetrievalService;
    
    public function __construct(
        CompanyRepository $companyRepository,
        CompanyRetrievalService $companyRetrievalService
    ){
        $this->companyRepository = $companyRepository;
        $this->companyRetrievalService = $companyRetrievalService;
    }

}