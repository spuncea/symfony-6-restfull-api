<?php

namespace App\Service\Company;

use App\Entity\Company;
use App\Service\BaseService;

class CompanyPersistenceService extends BaseService
{
    /**
     * Create a new company 
     * 
     * @param Company $company
     */
    public function create(Company $company)
    {
        $this->companyRepository->save($company, true);
    }

    public function delete(Company $company)
    {
        $this->companyRepository->remove($company, true);
    }
}