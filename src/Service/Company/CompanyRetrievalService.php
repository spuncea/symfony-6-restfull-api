<?php

namespace App\Service\Company;

use App\Entity\Company;
use App\Repository\CompanyRepository;

class CompanyRetrievalService
{

    protected CompanyRepository $companyRepository;
    
    public function __construct(CompanyRepository $companyRepository)
    {
        $this->companyRepository = $companyRepository;
    }

    public function retrieveOneById($id): ?Company
    {
        return $this->companyRepository->find($id);
    }

    public function getCompanies(): array
    {
        return $this->companyRepository->findAll();
    }
}