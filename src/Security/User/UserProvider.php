<?php

declare(strict_types=1);

namespace App\Security\User;

use App\Entity\User;
use App\Repository\UserRepository;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\Exception\UserNotFoundException;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Security\Core\User\PasswordUpgraderInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;

class UserProvider implements UserProviderInterface, PasswordUpgraderInterface
{
    private UserRepository $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }
    
    public function loadUserByIdentifier(string $identifier): UserInterface
    {
        $user = $this->userRepository->findOneBy(['email' => $identifier]);
        
        if($user === null) {
            throw new UserNotFoundException(sprintf('User with email %s not found', $identifier));
        }
        return $user;
    }

    public function loadUserByUsername(string $username): UserInterface
    {
        return $this->findUser($username);
    }

    public function refreshUser(UserInterface $user): UserInterface
    {
        if (!$user instanceof User) {
            throw new UnsupportedUserException(\sprintf('Instances of %s are not supported', \get_class($user)));
        }

        return $this->loadUserByUsername($user->getUsername());
    }

    private function findUser(string $username): UserInterface
    {
        $user = $this->userRepository->findOneByEmailOrFail($username);

        if (null === $user) {
            throw new UsernameNotFoundException(\sprintf('User with email %s not found', $username));
        }

        return $user;
    }
    
    public function upgradePassword(PasswordAuthenticatedUserInterface $user, string $newEncodedPassword): void
    {
        if(!$user instanceof User)  {
            throw new \Exception('Unsuported type of user', \get_class($user));
        }
        $user->setPassword($newEncodedPassword);

        $this->userRepository->save($user);
    }

    public function supportsClass(string $class)
    {
        return User::class === $class;
    }
}
