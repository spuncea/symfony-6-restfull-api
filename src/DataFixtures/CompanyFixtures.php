<?php

namespace App\DataFixtures;

use App\Entity\Company;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class CompanyFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        $company = $manager->getRepository(Company::class)->find(1);
        
        // no need to recreate the main company if it exists already
        if($company instanceof Company) {
            return;
        }
        
        $mainCompany = new Company();
        $mainCompany->setName('Demo');
        $manager->persist($mainCompany);

        $manager->flush();
    }
}
