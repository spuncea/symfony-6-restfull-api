<?php

namespace App\DataFixtures;

use App\Entity\Company;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class UserFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        $company = $manager->getRepository(Company::class)->find(1);
        
        if(!$company instanceof Company) {
            return;
        }

        $mainUser = $manager->getRepository(User::class)->find(1);
        
        // user already exists
        if($mainUser instanceof User) {
            return;
        }
        
        $mainUser = new User();
        $mainUser->setName('Symfony 6');
        $mainUser->setEmail('restfulapi@example.com');
        $mainUser->setPassword(password_hash('password', PASSWORD_BCRYPT));
        $mainUser->setCompany($company);
        $manager->persist($mainUser);

        $manager->flush();
    }
}
