<?php

namespace App\Api\Project;

use App\Api\BaseApiController;
use App\Entity\Project;
use App\Entity\User;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Validator\Constraints\GreaterThan;
use App\Api\Response\ApiResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\Constraints\Collection;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Required;

class ProjectController extends BaseApiController
{
    
    public function create(Request $request): JsonResponse
    {
        $requestData = json_decode((string) $request->getContent(), true);
        $constraints = new Collection([
            'name' => [new NotBlank(), new Required()],
            'userId' => [new Required(), new GreaterThan(0)],
        ]);

        $this->validate($requestData, $constraints);
        

        try {
            $user = $this->userRetrievalService->retrieveOneById($requestData['userId']);
        } catch (\Exception $exc) {
            return new ApiResponse(
                "Retrieval Error",
                null,
                ["Could not retrieve user with id ${requestData['userId']}"],
                Response::HTTP_BAD_REQUEST
            );
        }

        if(!$user instanceof User) {
            return new ApiResponse(
                "User not found",
                null,
                ["The user with id ${requestData['userId']} was not found"],
            );
        }
        
        $project = new Project();
        $project->setName($requestData['name']);
        $project->setUser($user);

        try {
            $this->projectPersistenceService->createProject($project);
        }catch (\Exception $exc) {
            return new ApiResponse(
                "Persistence Error",
                null,
                ["Could not create project ${requestData['name']}"],
                Response::HTTP_BAD_REQUEST
            );
        } 
        
        return new ApiResponse(
            'Successfully create new project',
            $this->projectDataTransformer->transformOne($project)
        );
    }

    public function delete($id)
    {
        $constraints = new Collection([
            'id' => [new NotBlank(), new Required(), new GreaterThan(0)],
        ]);

        $this->validate(['id' => $id], $constraints);

        try {
            $project = $this->projectRetrievalService->retrieveOneById($id);
        } catch (\Exception $exc) {
            return new ApiResponse(
                "Retrieval Error",
                null,
                ["Could not retrieve company with id ${id}"],
                Response::HTTP_BAD_REQUEST
            );
        }

        if(!$project instanceof Project) {
            return new ApiResponse(
                "Project not found",
                null,
                ["The project with id ${id} was not found"],
            );
        }

        try {
            $this->projectPersistenceService->deleteProject($project);
        } catch (\Exception $exc) {
            return new ApiResponse(
                "Delete Error",
                null,
                ["Could not delete project with id ${id}"],
                Response::HTTP_BAD_REQUEST
            );
        }

        return new ApiResponse("Project with id ${id} was deleted");
    }
}