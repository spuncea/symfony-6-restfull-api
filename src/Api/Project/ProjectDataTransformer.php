<?php

namespace App\Api\Project;

use App\Entity\Project;

class ProjectDataTransformer
{
    
    public function transformOne($project): array
    {
        return [
            'id' => $project->getId(),
            'name' => $project->getName(),
        ];
    }

    public function transformMany($projects): array
    {
        return array_map(function (Project $project) {
            return [
                'id' => $project->getId(),
                'name' => $project->getName(),
              
            ];
        }, $projects->getValues());
    }

}