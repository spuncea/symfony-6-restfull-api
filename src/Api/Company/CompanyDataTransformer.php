<?php

namespace App\Api\Company;

use App\Entity\Company;

class CompanyDataTransformer
{

    public function transformOne(Company $company)
    {
        return [
          'companyId' => $company->getId(),
          'name' => $company->getName()  
        ];
    }

    public function transformMany($companies): array
    {
        return array_map(function (Company $company) {
            return $this->transformOne($company);
        }, $companies);
    }
}