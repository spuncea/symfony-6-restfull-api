<?php

namespace App\Api\Company;

use App\Api\BaseApiController;
use App\Api\Response\ApiResponse;
use App\Entity\Company;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\Constraints\Collection;
use Symfony\Component\Validator\Constraints\GreaterThan;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Required;

class CompanyController extends BaseApiController
{

    public function list()
    {
        $this->logger->info('Retrieving a list of companies');
        try {
            $companies = $this->companyRetrievalService->getCompanies();
            $companies = $this->companyDataTransformer->transformMany($companies);
        } catch (\Throwable $exc) {
            $this->logger->error('Error while retrieving the list of companies');
            $this->logger->error($exc->getMessage());
            $this->logger->error($exc->getTraceAsString());
            return new ApiResponse(
                'Could not retrieve companies',
                null,
                ['Internal server error'],
                Response::HTTP_INTERNAL_SERVER_ERROR
            );
        }

        return new ApiResponse("Successfully retrieved " . count($companies) . " companies(s)",$companies);
    }
    
    public function create(Request $request)
    {
        $requestData = json_decode((string) $request->getContent(), true);
        $constraints = new Collection([
            'name' => [new NotBlank(), new Required()],
        ]);
        
        $this->validate($requestData, $constraints);
        
        $company = new Company();
        $company->setName($requestData['name']);

        try {
            $this->companyPersistenceService->create($company);
        } catch (\Exception $exc) {
            return new ApiResponse(
                "Persistence Error",
                null,
                ["Could not create company ${requestData['name']}"],
                Response::HTTP_BAD_REQUEST
            );
        }
        
        return new ApiResponse(
          "Successfully created company ${requestData['name']}",
          $this->companyDataTransformer->transformOne($company)  
        );
    }

    public function delete($id)
    {
        $constraints = new Collection([
            'id' => [new NotBlank(), new Required(), new GreaterThan(0)],
        ]);

        $this->validate(['id' => $id], $constraints);

        try {
            $company = $this->companyRetrievalService->retrieveOneById($id);
        } catch (\Exception $exc) {
            return new ApiResponse(
                "Retrieval Error",
                null,
                ["Could not retrieve company with id ${id}"],
                Response::HTTP_BAD_REQUEST
            );
        }

        if(!$company instanceof Company) {
            return new ApiResponse(
                "Company not found",
                null,
                ["The company with id ${id} was not found"],
            );
        }

        try {
            $this->companyPersistenceService->delete($company);
        } catch (\Exception $exc) {
            return new ApiResponse(
                "Delete Error",
                null,
                ["Could not delete company with id ${id}"],
                Response::HTTP_BAD_REQUEST
            );
        }

        return new ApiResponse("Company with id ${id} was deleted");
    }
}