<?php

namespace App\Api;

use App\Api\Project\ProjectDataTransformer;
use App\Api\User\UserDataTransformer;
use App\Service\Company\CompanyRetrievalService;
use App\Service\Project\ProjectPersistenceService;
use App\Service\Project\ProjectRetrievalService;
use App\Service\User\UserPersistenceService;
use App\Service\User\UserRetrievalService;
use Psr\Log\LoggerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use App\Api\Company\CompanyDataTransformer;
use App\Api\Response\ApiResponse;
use App\Service\Company\CompanyPersistenceService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;

class BaseApiController extends AbstractController
{
    public UserRetrievalService $userRetrievalService;
    public UserPersistenceService $userPersistenceService;
    public CompanyRetrievalService $companyRetrievalService;
    public LoggerInterface $logger;
    public UserDataTransformer $userDataTransformer;
    protected ValidatorInterface $validator;
    protected CompanyPersistenceService $companyPersistenceService;
    protected CompanyDataTransformer $companyDataTransformer;
    protected ProjectPersistenceService $projectPersistenceService;
    protected ProjectDataTransformer $projectDataTransformer;
    protected ProjectRetrievalService $projectRetrievalService;
    
    public function __construct(
        ValidatorInterface $validator,
        CompanyPersistenceService $companyPersistenceService,
        CompanyDataTransformer $companyDataTransformer,
        UserRetrievalService $retrievalService,
        UserPersistenceService $persistenceService,
        CompanyRetrievalService $companyRetrievalService,
        LoggerInterface $logger,
        UserDataTransformer $userDataTransformer,
        ProjectPersistenceService $projectPersistenceService,
        ProjectDataTransformer $projectDataTransformer,
        ProjectRetrievalService $projectRetrievalService
    ){
        $this->validator = $validator;
        $this->companyPersistenceService = $companyPersistenceService;
        $this->companyDataTransformer = $companyDataTransformer;
        $this->logger = $logger;
        $this->companyRetrievalService = $companyRetrievalService;
        $this->userRetrievalService = $retrievalService;
        $this->userPersistenceService = $persistenceService;
        $this->userDataTransformer = $userDataTransformer;
        $this->projectPersistenceService = $projectPersistenceService;
        $this->projectDataTransformer = $projectDataTransformer;
        $this->projectRetrievalService = $projectRetrievalService;
    }
    
    protected function validate($data, $constraints)
    {
        $violations = $this->validator->validate($data, $constraints);

        if(count($violations) > 0) {
            $errorMessages = [];
            foreach ($violations as $violation) {
                $errorMessages[] = $violation->getPropertyPath() . ' ' . $violation->getMessage();
            }
            return new ApiResponse(
                'Validation error',
                $data,
                $errorMessages,
                Response::HTTP_BAD_REQUEST
            );
        }
    }
    
}