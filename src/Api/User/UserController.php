<?php

namespace App\Api\User;

use App\Api\BaseApiController;
use App\Api\Response\ApiResponse;
use App\Entity\Company;
use App\Entity\User;
use App\Service\Company\CompanyRetrievalService;
use App\Service\User\UserPersistenceService;
use App\Service\User\UserRetrievalService;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\Constraints\Collection;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\GreaterThan;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Required;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class UserController extends BaseApiController
{
    /**
     * Retrieve a list of users 
     * 
     * @return JsonResponse
     */
    public function list(): JsonResponse
    {
        $this->logger->info('Retrieving a list of users');
        try {
            $users = $this->userRetrievalService->getUsers();
        } catch (\Throwable $exc) {
            $this->logger->error('Error while retrieving the list of users');
            $this->logger->error($exc->getMessage());
            $this->logger->error($exc->getTraceAsString());
            return new ApiResponse(
                'Could not retrieve users', 
                null, 
                ['Internal server error'], 
                Response::HTTP_INTERNAL_SERVER_ERROR
            );
        }
        
        return new ApiResponse("Successfully retrieved " . count($users) . " user(s)",$users);
    }

    public function create(Request $request): JsonResponse
    {
        $requestData = json_decode((string) $request->getContent(), true);
        $constraints = new Collection([
            'name' => [new NotBlank(), new Required()],
            'email' => [new Email(), new Required()],
            'password' => [new NotBlank(),new Required()],
            'companyId' => [new Required()]
        ]);

        $this->validate($requestData, $constraints);
        
        try {
            $company = $this->companyRetrievalService->retrieveOneById($requestData['companyId']);
        } catch (\Exception $exc) {
            return new ApiResponse(
                "Could not retrieve company with id ${requestData['companyId']}",
                null,
                ['Internal server error'],
                Response::HTTP_INTERNAL_SERVER_ERROR
            );
        }
        
        if(!$company instanceof Company) {
            return new ApiResponse(
                "Validation error",
                null,
                ["Company with id ${requestData['companyId']} was not found"],
                Response::HTTP_BAD_REQUEST
            );
        }

        try {
            $user = $this->userRetrievalService->retrieveOneByEmail($requestData['email']);
        } catch (\Exception $exc) {
            return new ApiResponse(
                "Validation error",
                null,
                ["Could not create user: " . $exc->getMessage()],
                Response::HTTP_BAD_REQUEST
            );
        }
        
        if($user instanceof User) {
            return new ApiResponse(
                "Validation error",
                null,
                ["User with email ${requestData['email']} already exists" ],
                Response::HTTP_BAD_REQUEST
            );
        }
        
        $user = new User();
        $user->setName($requestData['name']);
        $user->setEmail($requestData['email']);
        $user->setCompany($company);
        $user->setPassword(password_hash($requestData['password'], PASSWORD_BCRYPT));

        try {
             $this->userPersistenceService->createUser($user);
        } catch (\Exception $exc) {
            return new ApiResponse(
                "Validation error",
                null,
                ["Could not create user: " . $exc->getMessage()],
                Response::HTTP_BAD_REQUEST
            );
        }
        
        return new ApiResponse("Successfully created user", $requestData);
    }

    public function read($id): JsonResponse
    {

        $constraints = new Collection([
            'id' => [new NotBlank(), new Required(), new GreaterThan(0)],
        ]);

        $this->validate(['id' => $id], $constraints);
        
        try {
            $user = $this->userRetrievalService->retrieveOneById($id);
        } catch (\Exception $exc) {
            return new ApiResponse(
                "Retrieval Error",
                null,
                ["Could not retrieve user with id ${id}"],
                Response::HTTP_BAD_REQUEST
            );
        }
        
        if(!$user instanceof User) {
            return new ApiResponse(
                "User not found",
                null,
                ["The user with id ${id} was not found"],
            );
        }
        
        return new ApiResponse(
            "User retrieved",
            $this->userDataTransformer->transformOne($user)
        );
    }

    public function delete($id)
    {
        $constraints = new Collection([
            'id' => [new NotBlank(), new Required(), new GreaterThan(0)],
        ]);

        $this->validate(['id' => $id], $constraints);

        try {
            $user = $this->userRetrievalService->retrieveOneById($id);
        } catch (\Exception $exc) {
            return new ApiResponse(
                "Retrieval Error",
                null,
                ["Could not retrieve user with id ${id}"],
                Response::HTTP_BAD_REQUEST
            );
        }

        if(!$user instanceof User) {
            return new ApiResponse(
                "User not found",
                null,
                ["The user with id ${id} was not found"],
            );
        }

        try {
            $this->userPersistenceService->deleteUser($user);
        } catch (\Exception $exc) {
            return new ApiResponse(
                "Delete Error",
                null,
                ["Could not delete user with id ${id}"],
                Response::HTTP_BAD_REQUEST
            );
        }
        
        return new ApiResponse("User with id ${id} was deleted");
    }

    public function update(Request $request, $id)
    {
        $requestData = json_decode((string) $request->getContent(), true);
        $constraints = new Collection([
            'id' => [new NotBlank(), new Required(), new GreaterThan(0)],
            'name' => [new NotBlank()],
            'email' => [new Email()],
            'password' => [new NotBlank()],
        ]);
        
        $requestData['id'] = $id;

        $this->validate($requestData, $constraints);

        try {
            $userToUpdate = $this->userRetrievalService->retrieveOneById($id);
        } catch (\Exception $exc) {
            return new ApiResponse(
                "Validation error",
                null,
                ["Could not update user: " . $exc->getMessage()],
                Response::HTTP_BAD_REQUEST
            );
        }
        
        if(!$userToUpdate instanceof User) {
            return new ApiResponse(
                "User not found",
                null,
                ["The user with id ${id} was not found"],
            );
        }
        
        if(array_key_exists('email', $requestData)) {
            try {
                $user = $this->userRetrievalService->retrieveOneByEmail($requestData['email']);
            } catch (\Exception $exc) {
                return new ApiResponse(
                    "Validation error",
                    null,
                    ["Could not update user: " . $exc->getMessage()],
                    Response::HTTP_BAD_REQUEST
                );
            }
            

            if($user instanceof User && $user->getId() !==  (int) $id) {
                return new ApiResponse(
                    "Validation error",
                    null,
                    ["User with email ${requestData['email']}  already exists" ],
                    Response::HTTP_BAD_REQUEST
                );
            }
        }
        
        
        if(array_key_exists('name', $requestData)) {
            $userToUpdate->setName($requestData['name']);
        }
        if(array_key_exists('email', $requestData)) {
            $userToUpdate->setEmail($requestData['email']);
        }
        if(array_key_exists('password', $requestData)) {
            $userToUpdate->setPassword(password_hash($requestData['password'], PASSWORD_BCRYPT));
        }

        try {
            $this->userPersistenceService->updateUser($userToUpdate);
        } catch (\Exception $exc) {
            return new ApiResponse(
                "Validation error",
                null,
                ["Could not update user: " . $exc->getMessage()],
                Response::HTTP_BAD_REQUEST
            );
        }
        
        return new ApiResponse(
            "Successfully update user", 
            $this->userDataTransformer->transformOne($userToUpdate)
        );

    }
}