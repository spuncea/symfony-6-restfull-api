<?php

namespace App\Api\User;

use App\Api\Project\ProjectDataTransformer;
use App\Entity\User;

class UserDataTransformer
{
    protected ProjectDataTransformer $projectDataTransformer;
    
    public function __construct(ProjectDataTransformer $projectDataTransformer)
    {
        $this->projectDataTransformer = $projectDataTransformer;
    }
    
    public function transformList($data): array
    {
        return array_map(function (User $user) {
            return [
                'id' => $user->getId(),
                'name' => $user->getName(),
                'email' => $user->getEmail(),
                'company' => $user->getCompany()->getName(),
                'projects' => $this->projectDataTransformer->transformMany($user->getProjects())
            ];
        }, $data);
    }

    public function transformOne($user): array
    {
        return [
            'id' => $user->getId(),
            'name' => $user->getName(),
            'email' => $user->getEmail(),
            'company' => $user->getCompany()->getName(),
            'projects' =>  $this->projectDataTransformer->transformMany($user->getProjects())
        ];      
    }
}